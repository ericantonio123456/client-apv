<?php

namespace App\Entity;

use App\Repository\TableauxRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableauxRepository::class)]
class Tableaux
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $A = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $B = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $C = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $D = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $E = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $F = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $G = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $H = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $I = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $J = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $K = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $L = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $M = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $N = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $O = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $P = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $Q = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $R = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $S = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $T = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $U = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $V = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $W = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $X = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $Y = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $Z = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $AA = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $AB = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $AC = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $AD = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $AE = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $AF = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $AG = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $AH = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $AI = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getA(): ?string
    {
        return $this->A;
    }

    public function setA(?string $A): self
    {
        $this->A = $A;

        return $this;
    }

    public function getB(): ?string
    {
        return $this->B;
    }

    public function setB(?string $B): self
    {
        $this->B = $B;

        return $this;
    }

    public function getC(): ?string
    {
        return $this->C;
    }

    public function setC(?string $C): self
    {
        $this->C = $C;

        return $this;
    }

    public function getD(): ?string
    {
        return $this->D;
    }

    public function setD(?string $D): self
    {
        $this->D = $D;

        return $this;
    }

    public function getE(): ?string
    {
        return $this->E;
    }

    public function setE(?string $E): self
    {
        $this->E = $E;

        return $this;
    }

    public function getF(): ?string
    {
        return $this->F;
    }

    public function setF(?string $F): self
    {
        $this->F = $F;

        return $this;
    }

    public function getG(): ?string
    {
        return $this->G;
    }

    public function setG(?string $G): self
    {
        $this->G = $G;

        return $this;
    }

    public function getH(): ?string
    {
        return $this->H;
    }

    public function setH(?string $H): self
    {
        $this->H = $H;

        return $this;
    }

    public function getI(): ?string
    {
        return $this->I;
    }

    public function setI(?string $I): self
    {
        $this->I = $I;

        return $this;
    }

    public function getJ(): ?string
    {
        return $this->J;
    }

    public function setJ(?string $J): self
    {
        $this->J = $J;

        return $this;
    }

    public function getK(): ?string
    {
        return $this->K;
    }

    public function setK(?string $K): self
    {
        $this->K = $K;

        return $this;
    }

    public function getL(): ?string
    {
        return $this->L;
    }

    public function setL(?string $L): self
    {
        $this->L = $L;

        return $this;
    }

    public function getM(): ?string
    {
        return $this->M;
    }

    public function setM(?string $M): self
    {
        $this->M = $M;

        return $this;
    }

    public function getN(): ?string
    {
        return $this->N;
    }

    public function setN(?string $N): self
    {
        $this->N = $N;

        return $this;
    }

    public function getO(): ?string
    {
        return $this->O;
    }

    public function setO(?string $O): self
    {
        $this->O = $O;

        return $this;
    }

    public function getP(): ?string
    {
        return $this->P;
    }

    public function setP(?string $P): self
    {
        $this->P = $P;

        return $this;
    }

    public function getQ(): ?string
    {
        return $this->Q;
    }

    public function setQ(?string $Q): self
    {
        $this->Q = $Q;

        return $this;
    }

    public function getR(): ?string
    {
        return $this->R;
    }

    public function setR(?string $R): self
    {
        $this->R = $R;

        return $this;
    }

    public function getS(): ?string
    {
        return $this->S;
    }

    public function setS(?string $S): self
    {
        $this->S = $S;

        return $this;
    }

    public function getT(): ?string
    {
        return $this->T;
    }

    public function setT(?string $T): self
    {
        $this->T = $T;

        return $this;
    }

    public function getU(): ?string
    {
        return $this->U;
    }

    public function setU(?string $U): self
    {
        $this->U = $U;

        return $this;
    }

    public function getV(): ?string
    {
        return $this->V;
    }

    public function setV(?string $V): self
    {
        $this->V = $V;

        return $this;
    }

    public function getW(): ?string
    {
        return $this->W;
    }

    public function setW(?string $W): self
    {
        $this->W = $W;

        return $this;
    }

    public function getX(): ?string
    {
        return $this->X;
    }

    public function setX(?string $X): self
    {
        $this->X = $X;

        return $this;
    }

    public function getY(): ?string
    {
        return $this->Y;
    }

    public function setY(?string $Y): self
    {
        $this->Y = $Y;

        return $this;
    }

    public function getZ(): ?string
    {
        return $this->Z;
    }

    public function setZ(?string $Z): self
    {
        $this->Z = $Z;

        return $this;
    }

    public function getAA(): ?string
    {
        return $this->AA;
    }

    public function setAA(?string $AA): self
    {
        $this->AA = $AA;

        return $this;
    }

    public function getAB(): ?string
    {
        return $this->AB;
    }

    public function setAB(?string $AB): self
    {
        $this->AB = $AB;

        return $this;
    }

    public function getAC(): ?string
    {
        return $this->AC;
    }

    public function setAC(?string $AC): self
    {
        $this->AC = $AC;

        return $this;
    }

    public function getAD(): ?string
    {
        return $this->AD;
    }

    public function setAD(?string $AD): self
    {
        $this->AD = $AD;

        return $this;
    }

    public function getAE(): ?string
    {
        return $this->AE;
    }

    public function setAE(?string $AE): self
    {
        $this->AE = $AE;

        return $this;
    }

    public function getAF(): ?string
    {
        return $this->AF;
    }

    public function setAF(?string $AF): self
    {
        $this->AF = $AF;

        return $this;
    }

    public function getAG(): ?string
    {
        return $this->AG;
    }

    public function setAG(?string $AG): self
    {
        $this->AG = $AG;

        return $this;
    }

    public function getAH(): ?string
    {
        return $this->AH;
    }

    public function setAH(?string $AH): self
    {
        $this->AH = $AH;

        return $this;
    }

    public function getAI(): ?string
    {
        return $this->AI;
    }

    public function setAI(?string $AI): self
    {
        $this->AI = $AI;

        return $this;
    }
}
