<?php

namespace App\Controller;

use App\Entity\Fichier;
use App\Entity\Tableaux;
use App\Form\FichierType;
use App\Repository\FichierRepository;
use App\Repository\TableauxRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(Request $request, SluggerInterface $slugger, FichierRepository $fichierRepository, TableauxRepository $tableauxRepository, EntityManagerInterface $entityManager): Response
    {
        $fichier = new Fichier();
        $form = $this->createForm(FichierType::class, $fichier);
        $form->handleRequest($request);

        //Apres l'evoie du fichier
        if ($form->isSubmitted() && $form->isValid()) {

            // Enregistrer le fichier dans un dossier
            $fileData = $form->getData();
            $file = $form->get('fichier')->getData();
            if ($file) {
                $originalFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $saveFileName = $slugger->slug($originalFileName);
                $newFileName = $saveFileName . '-' . uniqid() . '.' . $file->guessExtension();
                try {
                    $file->move(
                        $this->getParameter('file_directory'),
                        $newFileName
                    );
                } catch (FileException $e) {
                }
                $fileData->setFichier($newFileName);
            }
            $fichierRepository->save($fichier, true);

            // Chemin vers le fichier
            $inputFileName = 'uploads/' . $newFileName;

            // Insertion du type de fichier
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');

            // Lecture du  fichier
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($inputFileName);

            // Index de la fin du colonne dans le fichier
            $index = $spreadsheet->getActiveSheet()->getCellCollection()->getCurrentCoordinate();

            $sheet = $spreadsheet->getActiveSheet()->rangeToArray(
                'A1:' . $index, NULL, TRUE, TRUE, TRUE);

            // Longueur du tableau
            $row = count($sheet);

            // Insertion dans la base de donner
            for ($i = 1; $i < $row+1; ++$i) {
                $tableau = new Tableaux();
                $tableau->setA($sheet[$i]['A'])
                    ->setB($sheet[$i]['B'])
                    ->setC($sheet[$i]['C'])
                    ->setD($sheet[$i]['D'])
                    ->setE($sheet[$i]['E'])
                    ->setF($sheet[$i]['F'])
                    ->setG($sheet[$i]['G'])
                    ->setH($sheet[$i]['H'])
                    ->setI($sheet[$i]['I'])
                    ->setJ($sheet[$i]['J'])
                    ->setK($sheet[$i]['K'])
                    ->setL($sheet[$i]['L'])
                    ->setM($sheet[$i]['M'])
                    ->setN($sheet[$i]['N'])
                    ->setO($sheet[$i]['O'])
                    ->setP($sheet[$i]['P'])
                    ->setQ($sheet[$i]['Q'])
                    ->setR($sheet[$i]['R'])
                    ->setS($sheet[$i]['S'])
                    ->setT($sheet[$i]['T'])
                    ->setU($sheet[$i]['U'])
                    ->setV($sheet[$i]['V'])
                    ->setW($sheet[$i]['W'])
                    ->setX($sheet[$i]['X'])
                    ->setY($sheet[$i]['Y'])
                    ->setZ($sheet[$i]['Z'])
                    ->setAA($sheet[$i]['AA'])
                    ->setAB($sheet[$i]['AB'])
                    ->setAC($sheet[$i]['AC'])
                    ->setAD($sheet[$i]['AD'])
                    ->setAE($sheet[$i]['AE'])
                    ->setAF($sheet[$i]['AF'])
                    ->setAG($sheet[$i]['AG'])
                    ->setAH($sheet[$i]['AH'])
                    ->setAI($sheet[$i]['AI']);
                $entityManager->persist($tableau);
            }
            $entityManager->flush();

            // Message de succee
            $this->addFlash(
                'success', 'Importation succès ! '
            );

            // Redirection
            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);

        } elseif ($form->isSubmitted()) {
            // Message d'erreur
            $this->addFlash(
                'danger', 'Vous avez rentrée  un mauvais format !'
            );
        }

        // Redirection
        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/tableau', name: 'app_tab')]
    public function tableau(TableauxRepository $tableauxRepository): Response
    {
        return $this->render('tableau/table.html.twig', [
            'tabs' => $tableauxRepository->findAll(),
        ]);
    }
}
