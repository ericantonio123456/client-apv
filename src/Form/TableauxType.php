<?php

namespace App\Form;

use App\Entity\Tableaux;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TableauxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('A')
            ->add('B')
            ->add('C')
            ->add('D')
            ->add('E')
            ->add('F')
            ->add('G')
            ->add('H')
            ->add('I')
            ->add('J',TextType::class,[
                'required' => false
            ])
            ->add('K')
            ->add('L')
            ->add('M')
            ->add('N')
            ->add('O')
            ->add('P')
            ->add('Q')
            ->add('R')
            ->add('S')
            ->add('T')
            ->add('U')
            ->add('V')
            ->add('W')
            ->add('X')
            ->add('Y')
            ->add('Z')
            ->add('AA')
            ->add('AB')
            ->add('AC')
            ->add('AD')
            ->add('AE')
            ->add('AF')
            ->add('AG')
            ->add('AH')
            ->add('AI')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tableaux::class,
        ]);
    }
}
